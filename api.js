/* DB connection */

const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/orizon';

(async function() {
    try {

        /* Requires */

        const client = await MongoClient.connect(uri,{ useNewUrlParser: true, useUnifiedTopology: true });
        const express = require('express');
        const cors = require('cors');
        const app = express();
        const bodyParser = require('body-parser');
        const auth = require('./resources/auth');
        const cameras = require('./resources/cameras');
        const collectionsModule = require('./models/collections');
        const db = await client.db();
        const collections = new collectionsModule(app, db);
        const port = 8001;

        /* Config */
        app.use(cors({
            origin: '*',
            optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
        }));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));

        /* Get Bearer token for auth */

        function managePrivileges(req, res, next) {

            const bearerHeader = req.headers["authorization"];

            if (typeof bearerHeader != 'undefined') {
                const bearer = bearerHeader.split(" ");
                const bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.statusCode = 401;
                res.json({
                    'error': 'Need correct API token in Bearer token'
                });
            }
        }

        /* Auth collections */

        collections.create("POST", '/orizon/v1/auth/login', (req, res, next) => { next(); }, auth.login);
        collections.create("POST", '/orizon/v1/auth/register', (req, res, next) => { next(); }, auth.register);

        collections.create("GET", '/orizon/v1/auth/connections', managePrivileges, auth.getConnections);
        collections.create("GET", '/orizon/v1/auth/myConnections', managePrivileges, auth.getMyConnections);
        collections.create("GET", '/orizon/v1/auth/users', managePrivileges, auth.getUsers);
        collections.create("GET", '/orizon/v1/auth/users/:username', managePrivileges, auth.getUser);
        collections.create("PUT", '/orizon/v1/auth/username/:username', managePrivileges, auth.putUsername);
        collections.create("PUT", '/orizon/v1/auth/mail/:mail', managePrivileges, auth.putMail);
        collections.create("PUT", '/orizon/v1/auth/password/:password', managePrivileges, auth.putPassword);
        collections.create("PUT", '/orizon/v1/auth/privileges/:username/:bool', managePrivileges, auth.putPrivileges);
        collections.create("DELETE", '/orizon/v1/auth/user/:username', managePrivileges, auth.deleteUser);

        /* Cameras collections */

        collections.create("POST", '/orizon/v1/cameras', managePrivileges, cameras.addCamera);
        collections.create("GET", '/orizon/v1/cameras/page/:n', managePrivileges, cameras.getNCameras);
        collections.create("GET", '/orizon/v1/cameras', managePrivileges, cameras.getCameras);
        collections.create("GET", '/orizon/v1/cameras/geojson', managePrivileges, cameras.getCamerasGeoJSON);
        collections.create("GET", '/orizon/v1/cameras/geojson/:id', managePrivileges, cameras.getCameraGeoJSON);
        collections.create("GET", '/orizon/v1/cameras/id/:id', managePrivileges, cameras.getCamera);
        collections.create("GET", '/orizon/v1/cameras/namespaces', managePrivileges, cameras.getNamespacesCamera);
        collections.create("GET", '/orizon/v1/cameras/bynamespaces/:namespace', managePrivileges, cameras.getCamerasByNamespaces);
        collections.create("GET", '/orizon/v1/cameras/username/:username', managePrivileges, cameras.getUserCameras);
        collections.create("DELETE", '/orizon/v1/cameras/:id', managePrivileges, cameras.deleteCamera);
        collections.create("PUT", '/orizon/v1/cameras/domain/:id', managePrivileges, cameras.updateDomain);
        collections.create("PUT", '/orizon/v1/cameras/description/:id', managePrivileges, cameras.updateDescription);

        /* HTTP listening */

        app.listen(port);

    } catch(e) {

        /* DB error connection */

        console.error(e);

    }

})()
