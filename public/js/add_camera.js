$(document).ready(function(){

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Events */

    var cameraForm = document.getElementById('addCameraForm');

    cameraForm.addEventListener('submit', function(e) {

        var token = getCookie("token");
        var urlField = document.getElementById("urlCamera");
        var urlSnapshotField = document.getElementById("urlSnapshotCamera");
        var namespaceField = document.getElementById("namespaceCamera");
        var descriptionField = document.getElementById("descriptionCamera");
        var tagsField = document.getElementsByName("hidden-tags")[0];
        var request = createCORSReq('POST', 'http://127.0.0.1:8001/orizon/v1/cameras');

        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                $("#successModal").modal('show');
            }
        };
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send("url=" + urlField.value + "&urlSnapshot=" + urlSnapshotField.value + "&namespace=" + namespaceField.value + "&description=" + descriptionField.value + "&tags=" + tagsField.value);
        e.preventDefault();

    });
});