/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Config */

const token = getCookie("token");

/* Alarms Name */

var alarmsName = [];

window.onload = function() {
    var req = createCORSReq('GET', 'http://127.0.0.1:8003/vision/v1/alarms/all');

    req.onreadystatechange = function () {
        if(req.readyState === 4 && req.status === 200) {
            var responseJSON = JSON.parse(req.responseText).alarms;

            for (var i = 0; i < responseJSON.length; i++) {
                alarmsName.push(responseJSON[i].name);
            }
        }
    };
    req.setRequestHeader("Authorization", "Bearer " + token);
    req.send();
};

function alarmInDetection(alarmsName, detection) {
    for (var i = 0; i < detection.alarms.length; i++) {
        if (alarmsName.includes(detection.alarms[i][0]))
            return (true);
    }
    return (false);
}

/* Websockets dets from vision */

let wsVision = io.connect('http://127.0.0.1:8002');

wsVision.on('dets', function(data) {

	if (data.alarms && data.alarms.length > 0 && alarmInDetection(alarmsName, data)) {
		var monitoring = document.getElementById('monitoring');
		var tr = document.createElement('tr');
		var tdDomain = document.createElement('td');
		var tdURL = document.createElement('td');
		var tdDate = document.createElement('td');
		var tdAlarms = document.createElement('td');
		var tdVisionary = document.createElement('td');
		var date = new Date(data.date);
		var formatDate = 'le' + ("0" + date.getDate()).slice(-2) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' à ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

		tr.style.backgroundColor = 'rgba(0, 0, 0, 0.1)';
		tr.style.cursor = 'pointer';
		tdDomain.innerHTML = '<a target="_blank" href="/analyse_camera/' + data.assoc + '">' + data.domain + '</a>';
		tdURL.innerHTML = '<a target="_blank" href="'+ data.url + '">'+ data.url + '</a>';
		tdDate.innerHTML = '<a target="_blank" href="' + data.src + '">' + formatDate + '</a>';
		tdAlarms.innerHTML = '<ul style="margin: 0; padding: 0;">';
		data.alarms.forEach(function(alarm) {
			tdAlarms.innerHTML += '<li style="margin-left: 20px;"><a target="_blank" href="/area_camera/' + alarm[1] + '">' + alarm[0] + '</a></li>';
		});
		tdAlarms.innerHTML += '</ul>';
		tdVisionary.innerHTML = '<a name="visionary" href="#" class="btn btn-primary"><span class="fas fa-eye"></span> Visionner</a>';
		tr.appendChild(tdDomain);
		tr.appendChild(tdURL);
		tr.appendChild(tdDate);
		tr.appendChild(tdAlarms);
		tr.appendChild(tdVisionary);
		monitoring.prepend(tr);
	}
});

/* Viewer */

function getDatas(detection) {
    var data = [];
    var personne = 0;
    var velo = 0;
    var voiture = 0;
    var moto = 0;
    var bus = 0;
    var camion = 0;
    var sacados = 0;
    var sacamains = 0;
    var valise = 0;
    var animal = 0;

    for (var i = 0; i < detection.dets.length; i++) {
        if (detection.dets[i].class == 0)
            personne += 1;
        else if (detection.dets[i].class == 1)
            velo += 1;
        else if (detection.dets[i].class == 2)
            voiture += 1;
        else if (detection.dets[i].class == 3)
            moto += 1;
        else if (detection.dets[i].class == 4)
            bus += 1;
        else if (detection.dets[i].class == 5)
            camion += 1;
        else if (detection.dets[i].class == 6)
            sacados += 1;
        else if (detection.dets[i].class == 7)
            sacamains += 1;
        else if (detection.dets[i].class == 8)
            valise += 1;
        else if (detection.dets[i].class == 9)
            animal += 1;
    }
    data.push(personne);
    data.push(velo);
    data.push(voiture);
    data.push(moto);
    data.push(bus);
    data.push(camion);
    data.push(sacados);
    data.push(sacamains);
    data.push(valise);
    data.push(animal);
    return (data);
}

function drawArea(ctx, area) {
    var topCoords = [];

    if (area.coords[2] < 0) {
        topCoords.push(area.coords[0] + area.coords[2]);
        topCoords.push(area.coords[0]);
    } else {
        topCoords.push(area.coords[0]);
        topCoords.push(area.coords[0] + area.coords[2]);
    }
    ctx.strokeStyle = "#00FF00";
    if (area.coords[3] < 0) topCoords.push(area.coords[1] + area.coords[3]);
    else topCoords.push(area.coords[1]);
    if (area.name) {
        ctx.font = "normal 20px  cursive";
        ctx.strokeText(area.name, topCoords[0], topCoords[2] - 5);
    }
    ctx.lineWidth = 2;
    ctx.strokeRect(area.coords[0], area.coords[1], area.coords[2], area.coords[3]);
}

function classIntToString(classes) {
    if (classes == 0) return ('Personne');
    else if (classes == 1) return ('Vélo');
    else if (classes == 2) return ('Voiture');
    else if (classes == 3) return ('Moto');
    else if (classes == 4) return ('Bus');
    else if (classes == 5) return ('Camion');
    else if (classes == 6) return ('Sac à dos');
    else if (classes == 7) return ('Sac à main');
    else if (classes == 8) return ('Valise');
    else if (classes == 9) return ('Animal');
    else return ('Unknown');
}

function coordsRectify(x, y, w, h)
{
    if (w < 0)
    {
        x = x + w;
        w = w * -1;
    }
    if (h < 0)
    {
        y = parseInt(y + h);
        h = h * -1;
    }
    coords = [x, y, w, h];
    return (coords);
}

var canvas = document.getElementById('detectionsStats');
var imgCanvas = document.getElementById('imgCanvas');
var imgCanvasCtx = imgCanvas.getContext('2d');
var ctx = canvas.getContext('2d');
var visionaryBtns = document.getElementsByName('visionary');

visionaryBtns.forEach((visionary) => {

    /* Visionary Viewer */
    visionary.addEventListener('click', () => {
        var id = visionary.getAttribute("id").split('_');
        var request = createCORSReq('GET', 'http://127.0.0.1:8003/vision/v1/detections/cameras/' + id[0] + '/progressSec/' + id[1]);

        request.onreadystatechange = function () {

            if(request.readyState === 4 && request.status === 200) {
                var responseJSON = JSON.parse(request.response).detection;
                var imgObj = new Image();

                /* Get Img */
                imgObj.src = responseJSON.src;
                imgObj.onload = function() {
	                imgCanvas.width = imgObj.width;
	                imgCanvas.height = imgObj.height;
                    imgCanvasCtx.drawImage(imgObj, 0, 0);

                     /* Draw dets */
                    for (var i = 0; i < responseJSON.dets.length; i++) {
                        var coordsRectif = coordsRectify(responseJSON.dets[i].x, responseJSON.dets[i].y, responseJSON.dets[i].w, responseJSON.dets[i].h);

                        if (responseJSON.dets[i].class != -1) {
                            drawArea(imgCanvasCtx, {
                                coords: coordsRectif,
                                name: classIntToString(responseJSON.dets[i].class)
                            });
                        }
                    }
                }

                /* Get stats */
                var chart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Personne', 'Vélo', 'Voiture', 'Moto', 'Bus', 'Camion', 'Sac à dos', 'Sac à main', 'Valise', 'Animal'],
                        datasets: [{
                            label: 'Nombre de detections',
                            data: getDatas(responseJSON),
                            backgroundColor: "rgba(0, 255, 0, 0.3)"
                        }]
                    },
                    options : {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    userCallback: function(label, index, labels) {
                                        // when the floored value is the same as the value we have a whole number
                                        if (Math.floor(label) === label) {
                                            return label;
                                        }
                                    },
                                }
                            }]
                        },
                    }
                });

                /* Export PDF */
                var exportPDFBtn = document.getElementById('exportPDFBtn');

                exportPDFBtn.addEventListener('click', () => {
                    $("#waitModal").modal('show');
                    setTimeout(() => {
                        /* Generate PDF */
                        var req = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/export/' + id[0]);
                        var token = getCookie("token");

                        req.onreadystatechange = function () {
                            if(req.readyState === 4 && req.status === 200) {
                                document.location = JSON.parse(req.responseText).url;
                            }
                        };
                        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        req.setRequestHeader("Authorization", "Bearer " + token);
                        req.send("progressSec=" + id[1] + "&statsDataUrl=" + encodeURIComponent(canvas.toDataURL('image/png')));
                    }, 3000);
                });

            }
        };

        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send();
        $("#viewModal").modal('show');
    });
});