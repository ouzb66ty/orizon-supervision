/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function initMonitor(camerasUrl) {
	var monitorDiv = document.getElementById('monitor');

	camerasUrl.forEach(function(cameraUrl) {

		/* Grid position */
		var img = document.createElement('img');
		var height = 100 / Math.round(Math.sqrt(camerasUrl.length));
		var width;

		if (camerasUrl.length == 2) width = 50;
		else width = 100 / Math.round(camerasUrl.length / 2);
		img.src = cameraUrl;
		img.style.width = String(width) + "vw";
		img.style.height = String(height) + "vh";
		monitorDiv.appendChild(img);
	});
}

/* Monitor cameras */

var splitUrl = window.location.toString().split('/');
let request = createCORSReq('GET', 'http://127.0.0.1:8001/orizon/v1/cameras/bynamespaces/' + splitUrl[splitUrl.length - 1]);
let token = getCookie("token");

request.onreadystatechange = function () {
    if (request.readyState === 4) {
        if (request.status === 200) {
            let responseJSON = JSON.parse(request.responseText);
            var monitor = [];

            for (var i = 0; i < responseJSON.cameras.length; i++) {
                monitor.push(responseJSON.cameras[i].urlSnapshot);
            }
            initMonitor(monitor);
        };
    };
};
request.setRequestHeader("Authorization", "Bearer " + token);
request.send();