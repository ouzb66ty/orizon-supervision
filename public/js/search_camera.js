/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true);
    } else {
        req.open(method, url, true);
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const token = getCookie("token");

/* Get data */

function getCamera() {
    return new Promise((resolve) => {
        let request = createCORSReq('GET', 'http://127.0.0.1:8001/orizon/v1/cameras/');
        let cameras = [];

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    let responseJSON = JSON.parse(request.responseText);
                    responseJSON.cameras.forEach((item) => {
                        cameras.push(item);
                    });
                    resolve(cameras);
                };
           };
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send();
    });
};

function getActive() {
    return new Promise((resolve) => {
        let request = createCORSReq('GET', 'http://127.0.0.1:8002/vision/v1/active_cameras/');
        let activate = [];

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    let responseJSON = JSON.parse(request.responseText);
                    responseJSON.active_cameras.forEach((item) => {
                        if (item.jsonRes.isActive === true)
                            activate.push(item.jsonRes._id);
                    });
                    resolve(activate);
                };
            };
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send();
        });
};


(async function main() {
    var cameras = await getCamera();
    var activate = await getActive();
    var namespaces = [];
    var tags = [];
    var users = [];

    var nameList = (obj) => {
        let namespaces = [];

        obj.forEach((item) => {
            namespaces.push(item.namespace);
        });
        return namespaces;
    };

    var tagList = (obj) => {
        let tags = [];

        obj.forEach((item) => {
            item.tags.forEach((item1) => {
                if (tags.includes(item1) === false && item1 !== "")
                    tags.push(item1);
            })
        });
        return tags;
    };

    function userList(obj) {
        let users = [];

        obj.forEach((item) => {
            if (users.includes(item.username) === false && item.username !== "")
                users.push(item.username);
        });
        return users;
    };

    function makeNamesStr(names) {
        let str = "";

        names.forEach((item) => {
            str = str + '<option>' + item + '</option>';
        });
        return '<select class="form-control" id="input-select-name" style="height: 100%;">' + str + '</select>';
    };

    function makeTagsStr(names) {
        let str = "";

        names.forEach((item) => {
            str = str + '<option>' + item + '</option>';
        });
        return '<select id="multiTag" type="text" class="form-control multiselect" multiple="multiple" role="multiselect" id="input-select-tag" style="height: 100%;">' + str + '</select>';
    };

    function makeUsersStr(names) {
        let str = "";

        names.forEach((item) => {
            str = str + '<option>' + item + '</option>';
        });
        return '<select class="form-control" id="input-select-user" style="height: 100%;">' + str + '</select>';
    };

    namespaces = nameList(cameras);
    tags = tagList(cameras);
    users = userList(cameras);

    nameInput = makeNamesStr(namespaces);
    tagInput = makeTagsStr(tags);
    userInput = makeUsersStr(users);

    const searchBtns = document.getElementsByName("searchBtn");
    const searchBtnsList = Array.prototype.slice.call(searchBtns);
    var btnSelect = 0;

    var pickerStart;
    var pickerEnd;

    var dateInput = '<form>\
                        <div class="form-row">\
                            <div class="form-goup col-6 mr-0">\
                                <input type="text" id="datePickerStart" class="form-control" placeholder="Depuis: " style="min-height: 45px;">\
                            </div>\
                            <div class="form-goup col-6 ml-0">\
                                <input type="text" id="datePickerEnd" class="form-control" placeholder="Jusqu\'au: " style="min-height: 45px;">\
                            </div>\
                        </div>\
                    </form>';
    var statusInput = '<div class="styleStatus">\
                            <div>\
                            <label class="custom-control custom-radio custom-control-inline">\
                                <input type="radio" name="radio-inline" checked class="custom-control-input" id="analyseRadio" value="1"><span class="custom-control-label">En analyse</span>\
                            </label>\
                            <label class="custom-control custom-radio custom-control-inline">\
                                <input type="radio" name="radio-inline" class="custom-control-input" value="2"><span class="custom-control-label">En pause</span>\
                            </label>\
                            </div>\
                        </div>'

    searchBtnsList.forEach((option) => {
        var optBtn = option.id;

        option.addEventListener("click", (error) => {
            $("#choice").empty();
            $("#searchInput").empty();
            if (optBtn === "searchName") {
                $("#choice").append("Par nom");
                $("#searchInput").append(nameInput);
                btnSelect = 0;
            } else if (optBtn === "searchTag") {
                $("#choice").append("Par tag");
                $("#searchInput").append(tagInput);
                $("#multiTag").multiselect();
                $("button.multiselect.btn").css({"height":"45px"});
                btnSelect = 1;
            } else if (optBtn === "searchDate") {
                $("#choice").append("Par date");
                $("#searchInput").append(dateInput);
                btnSelect = 2;
                try {
                    pickerStart.remove();
                    pickerEnd.remove();
                } catch (e) {
                    console.error(e);
                }
                pickerStart = datepicker("#datePickerStart", {id: 1, 
                    formatter: (input, date, instance) => {
                        const value = date.toLocaleDateString();
                        input.value = value;
                    },
                    customDays: ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'], 
                    customMonths: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre']
                });
                pickerEnd = datepicker("#datePickerEnd", {id: 1,
                    formatter: (input, date, instance) => {
                        const value = date.toLocaleDateString();
                        input.value = value;
                    },
                    customDays: ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'], 
                    customMonths: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'] 
                });
            } else if (optBtn === "searchStatus") {
                $("#choice").append("Par status");
                $("#searchInput").append(statusInput);
                btnSelect = 4;
            } else if (optBtn === "searchUser") {
                $("#choice").append("Par utilisateur");
                $("#searchInput").append(userInput);
                btnSelect = 3;
            };
            error.preventDefault();
        });
    });

    function searchName(name) {
        return new Promise((resolve) => {
            cameras.forEach((item) => {
                if (item.namespace === name){
                    resolve([item._id]);
                }
            });
        });
    };

    function searchTag(tag) {
        return new Promise((resolve) => {
            let id = [];

            cameras.forEach((item) => {
                tag.forEach((item2) => {
                    if (item.tags.includes(item2) && !id.includes(item._id))
                        id.push(item._id);
                })
            });
            resolve(id);
        });
    };

    function searchUser(user) {
        return new Promise((resolve) => {
            let id = [];

            cameras.forEach((item) => {
                if (item.username === user)
                    id.push(item._id);
            });
            resolve(id);
        });
    };

    function searchStatus(status) {
        return new Promise((resolve) => {
            let others = [];
            let tmp = -1;

            cameras.forEach((item) => {
                others.push(item._id);
            });

            if (status === true)
                resolve(activate);
            else {
                activate.forEach((item1) => {
                    tmp = others.findIndex((item2) => {
                        if (item2 === item1)
                            return item1;
                    });
                    if (tmp !== -1)
                        others.splice(tmp, 1);
                });
                resolve(others);
            }
        });
    };

    function searchDate() {
        return new Promise((resolve) => {
            let id = [];
            let start = pickerStart.dateSelected;
            let end = pickerEnd.dateSelected;

            start.setHours(00);
            end.setHours(23);
            start.setMinutes(00);
            end.setMinutes(59);
            start.setSeconds(00);
            end.setSeconds(59);

            start = start.getTime();
            end = end.getTime();

            cameras.forEach((item) => {
                tmp = new Date(item.date.split(' ')[1].split('/').reverse().join('-') + 'T00:00:00.000Z');
                tmp = tmp.getTime();
                if (start <= tmp && tmp <= end) {
                    id.push(item._id);
                }
            });
            resolve(id);
        });
    }

    function getCamData(id) {
        return new Promise((resolve) => {
            let data = [];

            id.forEach((item1) => {
                cameras.forEach((item2) => {
                    if (item1 === item2._id) {
                        data.push(item2);
                    }
                });
            });

            for (var i = 0; i < data.length; i++) {
                if (activate.includes(data[i]._id) === true) {
                    data[i].isActive = true;
                } else {
                    data[i].isActive = false;
                }
            }
            resolve(data);
        });
    };

    function resRender(camdata) {
        let tmp1 = "";
        let tmp2 = "";
        let tmp3 = "";
        let render = "";

        return new Promise((resolve) => {
            camdata.forEach((item) => {
                if (item.isActive === true) {
                    tmp3 = "\" class=\"btn btn-outline-primary mb-1 btn-block\"><span class=\"fas fa-pause mr-2\"></span>Pause</button>";
                } else {
                    tmp3 = "\" class=\"btn btn-primary mb-1 btn-block\"><span class=\"fas fa-play mr-2\"></span>Analyser</button>";
                }
                tmp2 = "<div><button id=\"analyse" + item._id + "\" name=\"listAnalyzeBtn\"" + tmp3
                    +  "<button id=\"trash" + item._id + "\" class=\"btn btn-outline-danger btn-block mt-0\" name=\"trash\"><span class=\"fas fa-trash mr-2\"></span>Supprimer</button></div>"

                tmp1 = "<tr id=\"search"+ item._id +"\"><td><a href=\"/analyse_camera/" + item._id + "\">" + item.namespace + "</a></td>" 
                + "<td><a href=\"/analyse_camera/" + item._id + "\">" + item.url + "</a></td>"
                + "<td>" + item.username + "</td>"
                + "<td>" + item.date + "</td>"
                + "<td>" + item.tags + "</td>"
                + "<td>" + tmp2 + "</td></tr>";
                render = render + tmp1;
            });
            resolve(render);
        });
    };

    async function createSearchList(id) {

        /** Display search list */

        let camdata = await getCamData(id);
        let header = '<div class="table-responsive">\
        <table id="filterTable" class="table table-striped table-bordered first">\
            <thead>\
                <tr>\
                    <th style=\"min-width: 100px;\">Nom</th>\
                    <th>URL</th>\
                    <th>Auteur</th>\
                    <th>Date d\'ajout</th>\
                    <th>tags</th>\
                    <th style=\"min-width: 150px;\">Actions</th>\
                </tr>\
            </thead>\
            <tbody>';
        let footer = '</tbody>\
                    </table>\
                </div>';
        let searchListHtml = await resRender(camdata);

        /*var oldCamBtn = document.getElementsByName("listAnalyzeBtn");
        console.log("oldBtn: " + oldCamBtn + "length: " + oldCamBtn.length);
        var oldBtnList = Array.prototype.slice.call(oldCamBtn);
        console.log("oldList: " + oldBtnList);*/

        /*oldBtnList.forEach((item) => {
            item.removeEventListener("click", );
        });*/

        $('#result').empty();
        $('#result').append(header + searchListHtml + footer);


        /** Analyse buttons */

        var newCamBtn = document.getElementsByName("listAnalyzeBtn");
        var newBtnList = Array.prototype.slice.call(newCamBtn);

        newBtnList.forEach((item) => {
            let id1 = item.id;

            item.addEventListener("click", function (e) {
                var request = createCORSReq('POST', 'http://127.0.0.1:8002/vision/v1/active_cameras');
                let cam = '#' + id1;
                let realId = id1.slice(7);

                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        if (request.status === 200) {
                            let responseJSON = JSON.parse(request.responseText);

                            if (responseJSON.success == 'new') {
                                $("#successModal").modal('show');
                                $(cam).empty();
                                $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                                $(cam).addClass("btn-outline-primary");
                                $(cam).removeClass("btn-primary");
                                activate.push(realId);
                            } else if (responseJSON.success == 'pause') {
                                $(cam).empty();
                                $(cam).append('<span id="analyzeStatIcon" class="fas fa-play"></span> Analyser');
                                $(cam).addClass("btn-primary");
                                $(cam).removeClass("btn-outline-primary");

                                for (let i = 0; i < activate.length; i++) {
                                    if (activate[i] === realId) {
                                        activate.splice(i, 1);
                                    }
                                }
                            } else if (responseJSON.success == 'resume') {
                                $(cam).empty();
                                $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                                $(cam).addClass("btn-outline-primary");
                                $(cam).removeClass("btn-primary");
                                activate.push(realId);
                                camdata.isActive = true;
                            }
                        };
                    };
                };
                request.setRequestHeader("Authorization", "Bearer " + token);
                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                request.send("id=" + realId);
                e.preventDefault();
            });
        });


        /* Trash buttons */

        const trashCam = document.getElementsByName("trash");
        const trashCamList = Array.prototype.slice.call(trashCam);

        trashCamList.forEach((item) => {
            var htmlId = item.id;
            var realId = htmlId.slice(5);

            item.addEventListener("click", function(e) {
                $("#removeModal").modal('show');

                var confTrash = document.getElementById("confTrash");
                confTrash.addEventListener("click", function(e) {
                    let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/cameras/' + realId);

                    req.onreadystatechange = function () {
                        if(req.readyState === 4 && req.status === 200) {
                            $('#search' + realId).remove();
                            $("#removeModal").modal('hide');
                        }
                    };
                    req.setRequestHeader("Authorization", "Bearer " + token);
                    req.send();
                });
                e.preventDefault();
            });
        });

    };

    (function submitSearch() {
        var analyzeCameras = null;
        $('#submitSearch').on('click', async () => {
            if (btnSelect === 0)
            {
                let id = await searchName($('#input-select-name').val());
                await createSearchList(id);
            }
            else if (btnSelect === 1)
            {
                let id = await searchTag($('#multiTag').val());
                createSearchList(id);
            }
            else if (btnSelect === 2)
            {
                let id = await searchDate();
                createSearchList(id);
            }
            else if (btnSelect === 3)
            {
                let id = await searchUser($('#input-select-user').val());
                createSearchList(id);
            }
            else if (btnSelect === 4)
            {
                let id = await searchStatus($('input#analyseRadio').prop( "checked" ));
                createSearchList(id);
            }
        });
    })();

    
    /** Code for  insta display */

    /** trash */

    const trashCam = document.getElementsByName("trash");
    const trashCamList = Array.prototype.slice.call(trashCam);

    trashCamList.forEach((item) => {
        var htmlId = item.id;
        var realId = htmlId.slice(5);

        item.addEventListener("click", function(e) {
            $("#removeModal").modal('show');

            var confTrash = document.getElementById("confTrash");
            confTrash.addEventListener("click", function(e) {
                let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/cameras/' + realId);

                req.onreadystatechange = function () {
                    if(req.readyState === 4 && req.status === 200) {
                        $('#search' + realId).remove();
                        $("#removeModal").modal('hide');
                    }
                };
                req.setRequestHeader("Authorization", "Bearer " + token);
                req.send();
            });
            e.preventDefault();
        });
    });
    
    /** Analyse */
    
    var newCamBtn = document.getElementsByName("listAnalyzeBtn");
    var newBtnList = Array.prototype.slice.call(newCamBtn);

    newBtnList.forEach((item) => {
        let id = item.id.slice(7);
        if(activate.includes(id)) {
            $('#' + item.id).empty();
            $('#' + item.id).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
            $('#' + item.id).removeClass('btn-primary');
            $('#' + item.id).addClass('btn-outline-primary');
        }
    });

    newBtnList.forEach((item) => {
        let id1 = item.id;

        item.addEventListener("click", function (e) {
            var request = createCORSReq('POST', 'http://127.0.0.1:8002/vision/v1/active_cameras');
            let cam = '#' + id1;
            let realId = id1.slice(7);

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    if (request.status === 200) {
                        let responseJSON = JSON.parse(request.responseText);

                        if (responseJSON.success == 'new') {
                            $("#successModal").modal('show');
                            $(cam).empty();
                            $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                            $(cam).addClass("btn-outline-primary");
                            $(cam).removeClass("btn-primary");
                            activate.push(realId);
                        } else if (responseJSON.success == 'pause') {
                            $(cam).empty();
                            $(cam).append('<span id="analyzeStatIcon" class="fas fa-play"></span> Analyser');
                            $(cam).addClass("btn-primary");
                            $(cam).removeClass("btn-outline-primary");

                            for (let i = 0; i < activate.length; i++) {
                                if (activate[i] === realId) {
                                    activate.splice(i, 1);
                                }
                            }
                        } else if (responseJSON.success == 'resume') {
                            $(cam).empty();
                            $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                            $(cam).addClass("btn-outline-primary");
                            $(cam).removeClass("btn-primary");
                            activate.push(realId);
                            camdata.isActive = true;
                        }
                    };
                };
            };
            request.setRequestHeader("Authorization", "Bearer " + token);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.send("id=" + realId);
            e.preventDefault();
        });
    });
})();