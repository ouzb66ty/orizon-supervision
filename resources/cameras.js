const ObjectID = require('mongodb').ObjectID;

module.exports = class Cameras {

    /**
     * @api {get} /orizon/v1/videos/username/{username} Get all videos by username
     * @apiName getVideos
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} videos All uploaded videos by username
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "_id": "5cab4ba22a645f864ed97066",
     *          "name": "Screencast from 18-06-2018 11_44_41.webm",
     *          "size": 2088052,
     *          "encoding": "7bit",
     *          "tempFilePath": "/tmp/tmp-1-1554729890178",
     *          "truncated": false,
     *          "mimetype": "video/webm",
     *          "md5": "93d774a9afa345b775240010caac8e32",
     *          "path": "uploads/Screencast from 18-06-2018 11_44_41.webm",
     *          "namespace": "yolo"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': 'Not access'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getUserCameras(db, req, res, user) {

        if (user.payload.is_admin == true) {

            db.collection('cameras').find({'username': req.params.username}).toArray(
                (err, cameras) => {

                    if (err) {

                        res.statusCode = 500;
                        res.json({
                            'error': 'Internal Server Error'
                        });
                        throw err;

                    } else {

                        res.json({cameras});

                    }

                });

        } else {

            res.statusCode = 401;
            res.json({'error': 'Not access'});

        }

    }

    /**
     * @api {post} /orizon/v1/cameras Add new camera
     * @apiName postCameras
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {String} label Label.
     * @apiParam {String} url Url.
     * @apiParam {String} description Description.
     * @apiParam {String} color Hex Color.
     *
     * @apiSuccess {Object} cameras All cameras
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'New camera add'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *          'error': "Bad URL"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static uniqid() {

        return (new Date().getTime() + Math.floor((Math.random() * 10000) + 1)).toString(16);

    }

    static addCamera(db, req, res, user) {

        const { URL } = require('url');
        const url = new URL(req.body.url);
        const date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();
        var request = {};

        if (url instanceof Error || url.protocol != 'rtsp:') {
            res.statusCode = 404;
            res.json({
                'error': 'Bad URL'
            });
        } else {

            db.collection('cameras').insertOne({
                url: url.href,
                urlSnapshot: req.body.urlSnapshot,
                protocol: url.protocol,
                namespace: req.body.namespace,
                description: req.body.description,
                pathSequence: req.body.namespace + '_' + Cameras.uniqid(),
                username: user.payload.username,
                date: 'le ' + date + '/' + month + '/' + year + ' à ' + hours + ':' + minutes + ':' + seconds,
                tags: req.body.tags.split(',')
            }, (err, dbResult) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({
                        'success': 'New camera add'
                    });

                }

            });

        }

    }

    /**
     * @api {get} /orizon/v1/cameras Get all cameras
     * @apiName getCameras
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} cameras All cameras
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "cameras": [{
     *              "_id": "5c917da654eb9e77915e23e2",
     *              "geojson": {
     *              "type": "Feature",
     *              "geometry": {
     *                  "type": "Point",
     *                  "coordinates": [
     *                      9.1979,
     *                      45.5888
     *                  ]
     *              },
     *              "properties": {
     *                  "url": "http://93.241.11.120:4567/cgi-bin/faststream.jpg",
     *                  "label": "Plage",
     *                  "color": "#000000",
     *                  "description": "",
     *                  "country": "IT",
     *                  "city": "Nova Milanese",
     *                  "zip": null,
     *                  "resgion": null,
     *                  "range": [
     *                      1406353408,
     *                      1406355455
     *                  ]
     *              }
     *          }]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getCameras(db, req, res, user)
    {
        var request = {};

        if (user.payload.is_admin == true) request = {};
        else request = { 'username' : user.payload.username };

        db.collection('cameras').find(request).toArray(
            (err, cameras) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({cameras});

                }

            });
    }

    static getCamerasByNamespaces(db, req, res, user)
    {
        var request = {};

        if (user.payload.is_admin == true) request = { 'namespace': req.params.namespace };
        else request = { 'username' : user.payload.username, 'namespace': req.params.namespace };

        db.collection('cameras').find(request).toArray(
            (err, cameras) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({cameras});

                }

            });
    }

    /**
     * @api {post} /orizon/v1/cameras/namespaces Get all namespaces camera
     * @apiName getNamespacesCamera
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} success Namespaces camera
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          ['domain1', 'domain2']
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getNamespacesCamera(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = {};
        else request = { 'username' : user.payload.username };

        db.collection('cameras').find(request).project({'namespace': 1, '_id': 0}).toArray(
            (err, namespaces) => {

                let valueArray = [];

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    let unique = {};

                    /* Filtering namespaces */
                    for (var i = 0; i < namespaces.length; i++) {
                        valueArray.push(namespaces[i].namespace);
                    }

                    /* Remove Dups */
                    valueArray.forEach(function(i) {
                        if(!unique[i]) {
                            unique[i] = true;
                        }
                    });
                    valueArray = Object.keys(unique);
                    res.json({'namespaces': valueArray});

                }

            });

    }

    /**
     * @api {get} /orizon/v1/cameras/page/{n} Get the N last page cameras
     * @apiName getNCameras
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {Number} n N Page (begin at 1) Cameras number to give.
     *
     * @apiSuccess {Object} cameras N page last cameras
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "cameras": [{
     *              "_id": "5c917da654eb9e77915e23e2",
     *              "geojson": {
     *              "type": "Feature",
     *              "geometry": {
     *                  "type": "Point",
     *                  "coordinates": [
     *                      9.1979,
     *                      45.5888
     *                  ]
     *              },
     *              "properties": {
     *                  "url": "http://93.241.11.120:4567/cgi-bin/faststream.jpg",
     *                  "label": "Plage",
     *                  "color": "#000000",
     *                  "description": "",
     *                  "country": "IT",
     *                  "city": "Nova Milanese",
     *                  "zip": null,
     *                  "resgion": null,
     *                  "range": [
     *                      1406353408,
     *                      1406355455
     *                  ]
     *              }
     *          }]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 409 Conflict
     *     {
     *          'error': "Page not found"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getNCameras(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = {};
        else request = { 'username' : user.payload.username };

        db.collection('cameras').countDocuments().then((count) => {

            let page = 6 * (req.params.n);

            if (page > count) {

                res.statusCode = 409;
                res.json({
                    'error': 'Page not found'
                });

            } else {

                db.collection('cameras').find(request).skip(page).limit(6).toArray(
                    (err, cameras) => {

                        if (err) {

                            res.statusCode = 500;
                            res.json({
                                'error': 'Internal Server Error'
                            });
                            throw err;

                        } else {

                            res.json({cameras});

                        }

                    });

            }

        });

    }

    /**
     * @api {get} /orizon/v1/cameras Get one camera
     * @apiName getCamera
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {String} id Camera ID.
     *
     * @apiSuccess {Object} camera One camera
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "_id": "5c917da654eb9e77915e23e2",
     *          "geojson": {
     *              "type": "Feature",
     *              "geometry": {
     *                  "type": "Point",
     *                  "coordinates": [
     *                      9.1979,
     *                      45.5888
     *                  ]
     *              },
     *              "properties": {
     *                  "url": "http://93.241.11.120:4567/cgi-bin/faststream.jpg",
     *                  "label": "Plage",
     *                  "color": "#000000",
     *                  "description": "",
     *                  "country": "IT",
     *                  "city": "Nova Milanese",
     *                  "zip": null,
     *                  "resgion": null,
     *                  "range": [
     *                      1406353408,
     *                      1406355455
     *                  ]
     *               }
     *          }
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getCamera(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) {
            request = { '_id': ObjectID(req.params.id) };
        } else {
            request = {
                'username' : user.payload.username,
                '_id': ObjectID(req.params.id)
            };
        }

        db.collection('cameras').findOne(request, (err, camera) => {

            if (err) {
                res.statusCode = 500;
                res.json({
                    'error': 'Internal Server Error'
                });
                throw err;
            }

            res.json(camera);

        });

    }


    /**
     * @api {delete} /orizon/v1/cameras Delete one camera
     * @apiName deleteCamera
     * @apiGroup cameras
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {String} id Camera ID.
     *
     * @apiSuccess {Object} success Camera deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Camera deleted'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static deleteCamera(db, req, res, user) {

        var requestAdmin = {};
        var request = require('request');

        if (user.payload.is_admin == true) requestAdmin = { '_id': new ObjectID(req.params.id) };
        else requestAdmin = { 'username' : user.payload.username, '_id': new ObjectID(req.params.id) };

        db.collection('cameras').deleteOne(requestAdmin, (err, dbResult) => {
            if (err) {
                res.statusCode = 500;
                res.json({'error': 'Internal Server Error'});
                throw err;
            } else {

                request.del({

                    url: 'http://127.0.0.1:8003/vision/v1/assoc/' + req.params.id,
                    auth: { 'bearer': req.token }

                });
                res.json({'success': 'Camera deleted'});
            }
        });

    }


    /**
     *
     *
     *
     */

    static updateDescription(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = { '_id': new ObjectID(req.params.id) };
        else request = { 'username' : user.payload.username, '_id': new ObjectID(req.params.id) };

        db.collection('cameras').updateOne(request, {$set: {"description" : req.body.description}});
        res.statusCode = 200;
        res.json({
            'success': 'Description correctly update'
        });
    }


    /**
     *
     *
     *
     */

    static updateDomain(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = { '_id': new ObjectID(req.params.id) };
        else request = { 'username' : user.payload.username, '_id': new ObjectID(req.params.id) };

        db.collection('cameras').updateOne(request, {$set: {"namespace" : req.body.domain}});
        res.statusCode = 200;
        res.json({
            'success': 'Domain correctly update'
        });
    }
};
