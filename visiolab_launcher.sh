#! /bin/sh

#VISIOLAB_DIR="/home/orizon/Desktop/SuperVision"
VISIOLAB_DIR=".." # Path to VisioLab
SUPNET_DIR="$VISIOLAB_DIR/vision/supnet"
VISION_DIR="$VISIOLAB_DIR/vision/"
ORIZON_DIR="$VISIOLAB_DIR/orizon/"
SUPSTREAM="$VISIOLAB_DIR/vision/supstream/supstream.py"

yad --title "VisioLab" --button=gtk-media-play:0 --button=gtk-media-stop:1 --button=gtk-delete:2
ANSWER=$?
if test "$ANSWER" = "0"
then
    python3 $SUPSTREAM 14000 &
    SUPSTREAM_PID=$!
    cd $SUPNET_DIR
    ./supnet-server &
    SUPNET_PID=$!
    yad --title "VisioLab" --text "Please wait for initialisation" --no-buttons --rtl --timeout=7 --timeout-indicator=top
    cd -
    cd $ORIZON_DIR
    node domain.js &
    DOMAIN_PID=$!
    node api.js &
    ORIZON_PID=$!
    cd -
    cd $VISION_DIR
    node api.js &
    VISION_PID=$!
    cd -
    echo $VISION_PID > $VISIOLAB_DIR/app_PIDs
    echo $ORIZON_PID >> $VISIOLAB_DIR/app_PIDs
    echo $DOMAIN_PID >> $VISIOLAB_DIR/app_PIDs
#    echo $SUPNET_PID >> $VISIOLAB_DIR/app_PIDs
    echo $SUPSTREAM_PID >> $VISIOLAB_DIR/app_PIDs
    yad --title "VisioLab" --text="VisioLab is ready. Go to localhost:8000"
elif test "$ANSWER" = "1"
then
    if test -e $VISIOLAB_DIR/app_PIDs
    then
	kill -9 $(head -1 $VISIOLAB_DIR/app_PIDs)
	kill -9 $(head -2 $VISIOLAB_DIR/app_PIDs | tail -1)
	kill -9 $(head -3 $VISIOLAB_DIR/app_PIDs | tail -1)
	kill -9 $(head -4 $VISIOLAB_DIR/app_PIDs | tail -1)
	rm $VISIOLAB_DIR/app_PIDs
    else
	yad --title "VisioLab" --text="VisioLab is not running"
    fi
elif test "$ANSWER" = "2"
then
    yad --title "VisioLab" --image=gtk-dialog-warning --text="Delete database: Are you sure?"
    DELETE=$?
    if test "$DELETE" = "0"
    then
	CHOISE=$(yad --height=184 --title "VisioLab" --list --checklist --column=Delete --column=Data 0 connections 1 cameras 2 active_cameras 3 detections 4 images)
	echo $CHOISE | grep cameras
	if test $? = 0
	then
	    mongo orizon --eval "printjson(db.cameras.drop())"
	fi
	echo $CHOISE | grep connections
	if test $? = 0
	then
	    mongo orizon --eval "printjson(db.connections.drop())"
	fi
	echo $CHOISE | grep active_cameras
	if test $? = 0
	then
	    mongo vision --eval "printjson(db.active_cameras.drop())"
	fi
	echo $CHOISE | grep detections
	if test $? = 0
	then
	    mongo vision --eval "printjson(db.detections.drop())"
	fi
	echo $CHOISE | grep images
	if test $? = 0
	then
	    rm -fr $VISION_DIR/public/sequences/*
	fi
    fi
fi
